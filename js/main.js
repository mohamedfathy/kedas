$(document).ready(function () {
  // ============================================  header ====================================
  function stickyHeader() {
      var header = $(".header");
      if ($(this).scrollTop() > 80) {
          header.addClass("sticky");
      } else {
          header.removeClass("sticky");
      }
  }
  stickyHeader();
  //sticky header
  $(window).on("scroll", function () {
      stickyHeader();
  });
  $(".header .navbar-toggler").addClass("collapsed");

  // scroll down btn
  $(".scroll-btn").on("click", function () {
      $("html").animate({
          scrollTop: $(".main-section").outerHeight()
      })
  });
});

// =========================================== google maps ===================================
function initMap() {
  var location = {lat: 30.095231, lng: 31.333115};
  // The map, centered at location
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 16, center: location, styles: [
        {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#e0efef"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "hue": "#1900ff"
                },
                {
                    "color": "#c0e8e8"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": 100
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": 700
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#7dcdcd"
                }
            ]
        }
    ]});
  // The marker, positioned at location
  var marker = new google.maps.Marker({position: location, map: map});
}
